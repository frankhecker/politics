# politics

This repository contains data files (in CSV format) and R code (in R
Markdown files) used to produce plots and related material for various
data analyses on political and related topics.
